import random
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import calendar
import tkinter as tk
from tkinter import messagebox
from tkcalendar import DateEntry


def calculate_age(dob, age_date, dob_time, age_time, lucky_output_digits):
    dob = datetime.strptime(dob, "%Y-%m-%d %H:%M:%S")
    age_date = datetime.strptime(age_date, "%Y-%m-%d %H:%M:%S")

    delta = relativedelta(age_date, dob)
    age_difference = age_date - dob
    leap_years = calendar.leapdays(dob.year, age_date.year)

    days = age_difference.days - leap_years
    years, days = divmod(days, 365)
    months, days = divmod(days, 30)

    input_number_str = str((age_difference.days * 24 + delta.hours) * 3600 +
                           delta.seconds)

    digit_sum = sum(int(char) for char in input_number_str)

    def generate_lucky_number(input_str, lucky_output_digits):

        digit_sum = sum(int(char) for char in input_number_str)
        #for char in input_str: ## same thing
        #digit_sum += int(char)
        x = digit_sum

        if len(str(digit_sum)) >= lucky_output_digits:
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sum = sum(int(char) for char in digit_sum_str)
            result = digit_sum

        else:
            print("Debug : seconds_digits > lucky_output_digits\n\n")
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sums = sum(int(char) for char in digit_sum_str)
                random_digits = ''.join(random.sample(input_number_str, 1))
                digit_sum = (random_digits) + str(digit_sum)
            result = digit_sum
        return result

    lucky_number = generate_lucky_number(input_number_str, lucky_output_digits)

    result_text = f"{delta.years} years {delta.months} months {delta.days} days\n" \
                  f"{delta.years * 12 + delta.months} months {delta.days} days\n" \
                  f"{age_difference.days // 7} weeks {age_difference.days % 7} days\n" \
                  f"{age_difference.days} days\n" \
                  f"{age_difference.days * 24 + delta.hours} hours\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 60 + delta.minutes} minutes\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 3600 + delta.seconds} seconds\n" \
                  f"{digit_sum} is the sum of the seconds string digits\n" \
                  f"{lucky_output_digits} is the output requirement digits\n" \
                  f"{lucky_number} is your lucky number"

    return result_text


def main():

    window = tk.Tk()
    window.title("Age Calculator")
    window.geometry("500x600")

    def open_calendar():
        dob_entry._calendar()

    def perform_age_calculation():
        dob_date_str = dob_entry.get()
        dob_time_str = dob_time_entry.get()
        age_date_str = age_date_entry.get()
        age_time_str = age_time_entry.get()
        dob_date_parts = dob_date_str.split('/')
        age_date_parts = age_date_str.split('/')
        if len(dob_date_parts[2]) == 2:
            dob_date_str = f"20{dob_date_parts[2]}-{dob_date_parts[0]}-{dob_date_parts[1]}"
        if len(age_date_parts[2]) == 2:
            age_date_str = f"20{age_date_parts[2]}-{age_date_parts[0]}-{age_date_parts[1]}"
        dob_str = f"{dob_date_str} {dob_time_str}"
        age_str = f"{age_date_str} {age_time_str}"

        lucky_output_digits = int(lucky_output_entry.get())

        result_text = calculate_age(dob_str, age_str, 0, 0,
                                    lucky_output_digits)
        result_label.config(text=result_text)

    dob_label = tk.Label(window, text="Enter your Date of Birth:")
    dob_label.grid(row=0, column=0, padx=10, pady=10, sticky='w')
    dob_entry = DateEntry(window, width=10, height=2)
    dob_entry.grid(row=0, column=1, padx=10, pady=10)

    dob_time_label = tk.Label(window,
                              text="Enter your Date of Birth Time: (HH:MM:SS)")
    dob_time_label.grid(row=1, column=0, padx=10, pady=10, sticky='w')
    dob_time_entry = tk.Entry(window)
    dob_time_entry.grid(row=1, column=1, padx=10, pady=10)
    dob_time_entry.insert(0, "00:00:00")

    age_date_label = tk.Label(window,
                              text="Enter the Date to Calculate Age at:")
    age_date_label.grid(row=2, column=0, padx=10, pady=10, sticky='w')
    age_date_entry = DateEntry(window)
    age_date_entry.grid(row=2, column=1, padx=10, pady=10)

    age_time_label = tk.Label(window, text="Enter the Time: (HH:MM:SS)")
    age_time_label.grid(row=3, column=0, padx=10, pady=10, sticky='w')
    age_time_entry = tk.Entry(window)
    age_time_entry.grid(row=3, column=1, padx=10, pady=10)
    age_time_entry.insert(0, "00:00:00")

    lucky_output_label = tk.Label(
        window, text="Enter the number of Lucky Output Digits:")
    lucky_output_label.grid(row=4, column=0, padx=10, pady=10, sticky='w')
    lucky_output_entry = tk.Entry(window)
    lucky_output_entry.grid(row=4, column=1, padx=10, pady=10)
    lucky_output_entry.insert(0, "6")

    calculate_button = tk.Button(window,
                                 text="Calculate Age",
                                 command=perform_age_calculation)
    calculate_button.grid(row=5, column=0, columnspan=2, padx=10, pady=10)

    result_label = tk.Label(window, text="")
    result_label.grid(row=6, column=0, columnspan=2, padx=10, pady=10)

    window.mainloop()


if __name__ == "__main__":
    main()
#
