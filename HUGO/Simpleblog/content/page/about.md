---
title: About
description: Everything about kazi sifat
date: '2021-07-28'
aliases:
  - about-us
  - about-hugo
  - contact
license: CC BY-NC-ND
lastmod: '2020-10-09'
menu:
    main: 
        weight: -90
        pre: user
---
write something here
Hugo makes use of a variety of open source projects including:

* https://github.com/yuin/goldmark

Websites built with Hugo are extremelly fast, secure and can be deployed anywhere including, AWS, GitHub Pages, Heroku, Netlify and any other hosting provider.

Learn more and contribute on [GitHub](https://github.com/gohugoio).
