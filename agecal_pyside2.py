import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QVBoxLayout, QWidget, QCalendarWidget, QTimeEdit
from datetime import datetime, timedelta
from PySide2.QtCore import QTime
from dateutil.relativedelta import relativedelta
from PySide2.QtWidgets import QSpinBox

from PySide2.QtGui import QClipboard
import random
import calendar


def calculate_age():
    dob_date = dob_calendar.selectedDate()
    age_date = age_calendar.selectedDate()

    dob_time = dob_time_selector.time()
    age_time = age_time_selector.time()

    dob = datetime(dob_date.year(), dob_date.month(), dob_date.day(), dob_time.hour(), dob_time.minute(), dob_time.second())
    age = datetime(age_date.year(), age_date.month(), age_date.day(), age_time.hour(), age_time.minute(), age_time.second())

    delta = relativedelta(age, dob)

    print("Date of Birth: ", dob)
    print("Age at the Date of: " , age)
    print("delta: ", delta)

    age_difference = age - dob
    #leap_years = sum(1 for year in range(dob.year, age.year + 1) if (year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)))
    leap_years = calendar.leapdays(dob.year, age.year)

    days = age_difference.days - leap_years #- leap_years

    years, days = divmod(days, 365)
    months, days = divmod(days, 30)


########################

    def generate_lucky_number(input_str, lucky_output_digits):

        digit_sum = sum(int(char) for char in input_number_str)
        #for char in input_str: ## same thing
            #digit_sum += int(char)
        x = digit_sum

        if len(str(digit_sum)) >= lucky_output_digits:
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sum = sum(int(char) for char in digit_sum_str)
            result = digit_sum

        else:
            print("Debug : seconds_digits > lucky_output_digits\n\n")
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sums = sum(int(char) for char in digit_sum_str)
                random_digits = ''.join(random.sample(input_number_str, 1))
                digit_sum = (random_digits) + str(digit_sum)
            result = digit_sum
        return result


    input_number_str = str(((age_difference.days * 24 + delta.hours) * 60 * 60 + delta.seconds))
    lucky_output_digits = lucky_digits_input.value()
    digit_sum = sum(int(char) for char in input_number_str)
    #print("The sum of the seconds string digits is:", digit_sum)
    #print("The lucky output digits is:" , lucky_output_digits)
    lucky_number = generate_lucky_number(input_number_str, lucky_output_digits)
    #print("your lucky number is", lucky_number)
#######################
    result_text = f"{delta.years} years {delta.months} months {delta.days} days\n" \
                  f"{delta.years * 12 + delta.months} months {delta.days} days\n" \
                  f"{age_difference.days // 7} weeks {age_difference.days % 7} days\n" \
                  f"{age_difference.days} days\n" \
                  f"{age_difference.days * 24 + delta.hours} hours\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 60 + delta.minutes} minutes\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 60 * 60 + delta.seconds} seconds\n"\
                  f"{(digit_sum)} is the sum of the seconds string digits \n"\
                  f"{(lucky_output_digits)} is the output requirement digits \n"\
                  f"{(lucky_number)} is your lucky number \n"\
                  #f"{(digit_sum)} \n"
    time_difference = dob_time.secsTo(age_time)
    hours = time_difference // 3600
    minutes = (time_difference % 3600) // 60

    cal_time_text = f"DOB - Age: {dob_time.toString('hh:mm')} - {age_time.toString('hh:mm')}  Time Difference: {hours} hours {minutes} minutes"
    result_text += cal_time_text
    print(result_text)
    result_label.setText(result_text)
app = QApplication(sys.argv)
window = QMainWindow()
window.setWindowTitle("Age Calculator")

widget = QWidget()
layout = QVBoxLayout()

dob_label = QLabel("Date of Birth:")
dob_label.setStyleSheet("font-size: 16px; font-weight: bold;")
dob_calendar = QCalendarWidget()
layout.addWidget(dob_label)
layout.addWidget(dob_calendar)

dob_time_selector = QTimeEdit()
dob_time_selector.setDisplayFormat("hh:mm:ss")
dob_time_selector.setTime(QTime(0, 0))
layout.addWidget(dob_time_selector)

age_label = QLabel("Age at the Date of:")
age_label.setStyleSheet("font-size: 16px; font-weight: bold;")
age_calendar = QCalendarWidget()
layout.addWidget(age_label)
layout.addWidget(age_calendar)

age_time_selector = QTimeEdit()
age_time_selector.setDisplayFormat("hh:mm:ss")
age_time_selector.setTime(QTime(0, 0))
layout.addWidget(age_time_selector)


lucky_digits_label = QLabel("Lucky Output Digits: (optional) ")
lucky_digits_label.setStyleSheet("font-size: 16px; font-weight: bold;")
lucky_digits_input = QSpinBox()
lucky_digits_input.setMinimum(1)
lucky_digits_input.setMaximum(99)
lucky_digits_input.setValue(6)
layout.addWidget(lucky_digits_label)
layout.addWidget(lucky_digits_input)

def set_current_datetime():
    current_datetime = datetime.now()
    age_time_selector.setTime(current_datetime.time())
    age_calendar.setSelectedDate(current_datetime.date())

set_current_datetime_button = QPushButton("Set system Date and Time to age and calculate")
set_current_datetime_button.clicked.connect(set_current_datetime)

set_current_datetime_button.clicked.connect(calculate_age)
layout.addWidget(set_current_datetime_button)

calculate_button = QPushButton("Calculate Age")
calculate_button.clicked.connect(calculate_age)
layout.addWidget(calculate_button)
def copy_text():
    clipboard = QApplication.clipboard()
    clipboard.setText(result_label.text())

result_label = QLabel("")
layout.addWidget(result_label)

copy_button = QPushButton("Copy Text")
copy_button.clicked.connect(copy_text)
layout.addWidget(copy_button)

widget.setLayout(layout)
window.setCentralWidget(widget)
window.show()

sys.exit(app.exec_())
