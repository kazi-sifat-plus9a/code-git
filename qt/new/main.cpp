#include <QCoreApplication>
#include <QDebug>

//QObject get_object(QString name)
//{
//    QObject o;
//    o.setObjectName(name);
//    return  o;
//}


//QObject& get_ref(QString name)
//{
//    QObject o;
//    o.setObjectName(name);
//    return  o;
//}
QObject* get_ptr(QString name)
{
    QObject *o = new QObject;
    o->setObjectName(name);
    return  o;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //QObject o12 = get_ref("ByRef");
    //QObject o2 = get_ref("ByRef");
    QObject *o3 = get_ptr("Byptr");
    qInfo() <<  o3 ->objectName();
    return a.exec();
}
