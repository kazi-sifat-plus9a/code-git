#dictionary
continents = {
    "Asia": ["China", "India", "Japan", "South Korea", "Indonesia", "Thailand", "Vietnam", "Pakistan", "Bangladesh", "Mongolia", "Malaysia", "Sri Lanka", "Nepal", "Cambodia", "Myanmar", "Russia"],
    "Africa": ["Nigeria", "South Africa", "Kenya", "Egypt", "Ghana", "Morocco", "Ethiopia", "Tanzania", "Uganda", "Algeria", "Tunisia", "Senegal", "Rwanda", "Cameroon", "Ivory Coast"],
    "North America": ["United States", "Canada", "Mexico", "Jamaica", "Costa Rica", "Honduras", "Guatemala", "Panama", "Belize", "El Salvador", "Nicaragua", "Haiti", "Dominican Republic", "Trinidad and Tobago"],
    "South America": ["Brazil", "Argentina", "Peru", "Chile", "Colombia", "Ecuador", "Venezuela", "Bolivia", "Paraguay", "Uruguay", "Guyana", "Suriname", "Falkland Islands", "French Guiana", "Ecuador"],
    "Antarctica": [],
    "Europe": ["France", "Germany", "Italy", "Spain", "Greece", "Sweden", "Netherlands", "Portugal", "Norway", "Switzerland", "Denmark", "Ireland", "Austria", "Belgium", "Poland"],
    "Australia (Oceania)": ["Australia", "New Zealand", "Fiji", "Papua New Guinea", "Samoa", "Tonga", "Solomon Islands", "Vanuatu", "New Caledonia", "Tuvalu", "Kiribati", "Micronesia", "Palau"]
}
cities = {
    "China": ["Beijing", "Shanghai", "Guangzhou", "Shenzhen", "Chengdu"],
    "India": ["Mumbai", "Delhi", "Bangalore", "Kolkata", "Chennai"],
    "Japan": ["Tokyo", "Osaka", "Kyoto", "Yokohama", "Sapporo"],
    "South Korea": ["Seoul", "Busan", "Incheon", "Daegu", "Daejeon"],
    "Indonesia": ["Jakarta", "Surabaya", "Bandung", "Medan", "Semarang"],
    "Thailand": ["Bangkok", "Chiang Mai", "Phuket", "Pattaya", "Nakhon Ratchasima"],
    "Vietnam": ["Ho Chi Minh City", "Hanoi", "Da Nang", "Nha Trang", "Can Tho"],
    "Pakistan": ["Karachi", "Lahore", "Islamabad", "Rawalpindi", "Faisalabad"],
    "Bangladesh": ["Dhaka", "Chittagong", "Khulna", "Rajshahi", "Barisal"],
    "Mongolia": ["Ulaanbaatar", "Erdenet", "Darkhan", "Khovd", "Olgii"],
    "Nigeria": ["Lagos", "Kano", "Ibadan", "Kaduna", "Port Harcourt"],
    "South Africa": ["Johannesburg", "Cape Town", "Durban", "Pretoria", "Bloemfontein"],
    "Kenya": ["Nairobi", "Mombasa", "Kisumu", "Nakuru", "Eldoret"],
    "Egypt": ["Cairo", "Alexandria", "Giza", "Shubra El-Kheima", "Port Said"],
    "Ghana": ["Accra", "Kumasi", "Tamale", "Sekondi-Takoradi", "Cape Coast"],
    "Morocco": ["Casablanca", "Rabat", "Fes", "Marrakech", "Tangier"],
    "Ethiopia": ["Addis Ababa", "Dire Dawa", "Mek'ele", "Gondar", "Hawassa"],
    "Tanzania": ["Dar es Salaam", "Dodoma", "Mwanza", "Arusha", "Zanzibar City"],
    "Uganda": ["Kampala", "Gulu", "Lira", "Mbarara", "Jinja"],
    "Algeria": ["Algiers", "Oran", "Constantine", "Annaba", "Blida"],
    "Tunisia": ["Tunis", "Sfax", "Sousse", "Kairouan", "Bizerte"],
    "Senegal": ["Dakar", "Thiès", "Kaolack", "Ziguinchor", "Rufisque"],
    "Rwanda": ["Kigali", "Butare", "Gitarama", "Ruhengeri", "Gisenyi"],
    "Cameroon": ["Douala", "Yaoundé", "Garoua", "Bamenda", "Maroua"],
    "Ivory Coast": ["Abidjan", "Bouaké", "Daloa", "Korhogo", "San Pedro"],
    "Russia" : ["Moscow", "Saint Petersburg", "Novosibirsk", "Yekaterinburg", "Kazan", "Nizhny Novgorod", "Chelyabinsk", "Omsk", "Samara", "Rostov-on-Don"],

}


print("Select a continent:")
for i, continent in enumerate(continents.keys(), start=1):
    print(f"{i}. {continent}")

continent_option = int(input("Enter the option (1, 2, 3, 4, 5, 6, 7): "))

if continent_option not in [1, 2, 3, 4, 5, 6, 7]:
    print("Invalid option.")
else:
    selected_continent = list(continents.keys())[continent_option - 1]

    print(f"Select a country in {selected_continent}:")
    countries = continents[selected_continent]
    for i, country in enumerate(countries, start=1):
        print(f"{i}. {country}")

    country_option = int(input(f"Enter the option (1, 2, 3, {len(countries)}): "))

    if country_option not in range(1, len(countries) + 1):
        print("Invalid option.")
    else:
        selected_country = countries[country_option - 1]



        if selected_country in cities:
            selected_city = cities[selected_country]

            print(f"Select a city in {selected_country}:")
            for i, city in enumerate(selected_city, start=1):
                print(f"{i}. {city}")

            city_option = int(input(f"Enter the option (1, 2, 3, {len(selected_city)}): "))

            if city_option not in range(1, len(selected_city) + 1):
                print("Invalid option.")
            else:
                selected_city = selected_city[city_option - 1]
                print(f"You selected {selected_city} in {selected_country} within {selected_continent}.")
                print(selected_continent + "/" + selected_country + "/" + selected_city)
                print(f"{selected_continent}/{selected_country}/{selected_city}")

        else:
            print(f"No city information available for {selected_country} within {selected_continent}.")




        print(f"You selected {selected_country} in {selected_continent}.")
        print(selected_continent + "/" + selected_country)



        import requests
        import pytz
        from datetime import datetime

        url = f"http://worldtimeapi.org/api/timezone/{selected_continent}/{selected_city}"
        response = requests.get(url)
        data = response.json()
        try:
            current_time = data['datetime']
            print(f"{city}: {current_time}")
        except KeyError:
            print(f"Error: Unable to retrieve current time for {selected_city} using worldtimeapi.org switching to pytz module ")
            print("\033[91mIf you still get error try other city or take help from Dev.\033[0m")

            #[print(timezone) for timezone in pytz.all_timezones]

            selected_continent = "Asia"
            selected_city = "Mumbai"
            a = selected_continent + "/" + selected_city
            the_timezone = pytz.timezone(a)
            current_time = datetime.now(the_timezone)
            print(f"The time in {a}: {current_time}")


